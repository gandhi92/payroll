-- phpMyAdmin SQL Dump
-- version 4.0.4
-- http://www.phpmyadmin.net
--
-- Inang: 127.0.0.1
-- Waktu pembuatan: 19 Feb 2018 pada 15.17
-- Versi Server: 5.5.32
-- Versi PHP: 5.4.16

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Basis data: `db_payroll`
--
CREATE DATABASE IF NOT EXISTS `db_payroll` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `db_payroll`;

-- --------------------------------------------------------

--
-- Struktur dari tabel `menus`
--

CREATE TABLE IF NOT EXISTS `menus` (
  `id_menu` int(11) NOT NULL AUTO_INCREMENT,
  `no` int(11) NOT NULL,
  `nama_menu` varchar(25) NOT NULL,
  `link_menu` varchar(50) NOT NULL,
  `icon_menu` varchar(35) NOT NULL,
  `parent_menu` int(11) NOT NULL,
  `active` int(11) NOT NULL,
  PRIMARY KEY (`id_menu`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=15 ;

--
-- Dumping data untuk tabel `menus`
--

INSERT INTO `menus` (`id_menu`, `no`, `nama_menu`, `link_menu`, `icon_menu`, `parent_menu`, `active`) VALUES
(6, 3, 'List Menu', 'Menu', 'fa-qrcode', 10, 1),
(10, 2, 'Menu', '#', 'fa-qrcode', 0, 1),
(11, 1, 'Dashboard', 'Index', 'fa-dashboard', 0, 1),
(13, 4, 'Menu Management', 'Menus_management', 'fa-book', 10, 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `menus_management`
--

CREATE TABLE IF NOT EXISTS `menus_management` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_user` int(11) NOT NULL,
  `id_menus` int(11) NOT NULL,
  `tambah` int(11) NOT NULL,
  `edit` int(11) NOT NULL,
  `delete` int(11) NOT NULL,
  `report` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=19 ;

--
-- Dumping data untuk tabel `menus_management`
--

INSERT INTO `menus_management` (`id`, `id_user`, `id_menus`, `tambah`, `edit`, `delete`, `report`) VALUES
(4, 1, 11, 1, 1, 1, 1),
(6, 1, 6, 1, 1, 1, 1),
(7, 1, 10, 1, 1, 1, 1),
(8, 1, 13, 1, 1, 1, 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `project`
--

CREATE TABLE IF NOT EXISTS `project` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(20) NOT NULL,
  `copyright` varchar(20) NOT NULL,
  `version` varchar(10) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data untuk tabel `project`
--

INSERT INTO `project` (`id`, `nama`, `copyright`, `version`) VALUES
(1, 'Payroll', 'L Si Edogawa', '0.1');

-- --------------------------------------------------------

--
-- Struktur dari tabel `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id_user` int(11) NOT NULL AUTO_INCREMENT,
  `login_user` varchar(25) NOT NULL,
  `password_user` text NOT NULL,
  `nama_user` varchar(25) NOT NULL,
  `last_login` datetime NOT NULL,
  PRIMARY KEY (`id_user`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data untuk tabel `users`
--

INSERT INTO `users` (`id_user`, `login_user`, `password_user`, `nama_user`, `last_login`) VALUES
(1, 'admin', '$2y$10$13jsE8Vz3S30rKEmT8Z2U.DeG.fAx1C9QCL7fZC.mTZGHBpmSdyJO', 'Galoeng', '2018-02-04 00:00:00'),
(2, 'hrd', '$2y$10$pWlL0QJNaha9XE2rNdV14.i4kY4txdPygbj4dMZJYEiJArh1oN8lq', 'HRD', '2018-02-04 00:00:00');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
