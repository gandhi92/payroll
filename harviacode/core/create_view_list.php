<?php

$string = "
<?php
 \$Ses = \$this->session->userdata;
 \$Con = \$Ses['controller'];
 \$Cre = \$Ses['data']['tambah'];
 \$Edi = \$Ses['data']['edit'];
 \$Del = \$Ses['data']['delete'];
 \$Rep = \$Ses['data']['report'];
?>
<div class='content-wrapper'>
  <section class='content-header'>
    <h1>
      Dashboard
      <small>".ucfirst($table_name)." List</small>
    </h1>
    <ol class='breadcrumb'>
      <li><a href='#'><i class='fa fa-dashboard'></i> Home</a></li>
      <li class='active'>".ucfirst($table_name)." List</li>
    </ol>
  </section>
  <section class='content'>
    <div class='row'>
      <div class='col-xs-12'>
        <div class='box box-solid box-info'>
          <div class='box-header with-border'>
            <h3 class='box-title'>".ucfirst($table_name)." List</h3>
          </div><!-- /.box-header -->
          <div class='box-body'>
            <div class=\"col-md-4\">";
                if ($export_excel == '1') {
                    $string .= "\n\t\t  | <?php echo anchor(site_url('".ucfirst($c_url)."/excel'), 'Excel', 'class=\"btn btn-primary\"'); ?>";
                }
                if ($export_word == '1') {
                    $string .= "\n\t\t<?php echo anchor(site_url('".ucfirst($c_url)."/word'), 'Word', 'class=\"btn btn-primary\"'); ?>";
                }
                if ($export_pdf == '1') {
                    $string .= "\n\t\t<?php echo anchor(site_url('".ucfirst($c_url)."/pdf'), 'PDF', 'class=\"btn btn-primary\"'); ?>";
                }
$string .="</div>";
$string .="<div class=\"col-md-4 text-center\">
                <div style=\"margin-top: 8px\" id=\"message\">
                    <?php echo \$this->session->userdata('message') <> '' ? \$this->session->userdata('message') : ''; ?>
                </div>
            </div>
            <div class=\"col-md-4 text-right\">
                <form action=\"<?php echo site_url('$c_url/index'); ?>\" class=\"form-inline\" method=\"get\">
                    <div class=\"input-group\">
                        <input type=\"text\" class=\"form-control\" name=\"q\" value=\"<?php echo \$q; ?>\">
                        <span class=\"input-group-btn\">
                            <?php
                                if (\$q <> '')
                                {
                                    ?>
                                    <a href=\"<?php echo site_url('$c_url'); ?>\" class=\"btn btn-default\">Reset</a>
                                    <?php
                                }
                            ?>
                          <button class=\"btn btn-primary\" type=\"submit\">Search</button>
                        </span>
                    </div>
                </form>
            </div>
          </div>
          <div class='box-body'>
            <table class='table table-hover'>
            <tr>
                <th>No</th>";
                foreach ($non_pk as $row) {
                    $string .= "\n\t\t<th>" . label($row['column_name']) . "</th>";
                }
            $string .="<?php echo (\$Edi!=1 && \$Del!=1 && \$Cre !=1)?\"\":\"<th>Action</th>\"; ?>";
            $string .="<?php if (\$Cre ==1) {?>
            <?php echo form_open(\"".ucfirst($c_url)."/create_action\"); ?><tr><td></td>";
                foreach ($non_pk as $row) {
            $string .="
                <td><input type=\"text\" name=\"".$row['column_name']."\" class=\"form-control input-sm\" placeholder=\"". label($row['column_name']) ."\" required></td>";
            }
            $string .="<td style=\"text-align:center\"><button type=\"submit\" name=\"tambah\" class=\"btn btn-sm btn-success\">Tambah</button></td></tr></form>";
            $string .= "<?php }  ?><?php foreach ($" . $c_url . "_data as \$$c_url){ ?><tr>";
            $string .= "\n\t\t\t<td width=\"30px\"><?php echo ++\$start ?></td>";
            $string .="<?php echo form_open(\"".ucfirst($c_url)."/update_action\"); ?>";
            $string .="<input type=\"hidden\" name=\"".$pk."\" value=\"<?php echo $".$c_url."->".$pk." ?>\" >";
            foreach ($non_pk as $row) {
                $string .= "\n\t\t\t<td>
                <c id=\"tx".$row['column_name']."<?php echo $" .$c_url."->".$pk." ?>\" style=\"text-decoration: none;\" ><?php echo $" .$c_url."->". $row['column_name'] . " ?></c>
                  <input id=\"fr".$row['column_name']."<?php echo $" .$c_url."->".$pk."; ?>\" type=\"hidden\" name=\"". $row['column_name'] . "\" class=\"form-control input-sm\" value=\"<?php echo $" .$c_url."->". $row['column_name'] . " ?>\" >
                </td>";

            }
            $string .= "\n\t\t\t<td style=\"text-align:center\" width=\"200px\">"
                    . "\n\t\t\t\t<?php "
                    ."echo (\$Edi==1 )?\"<button disabled id='ed$".$c_url."->".$pk."'  class='btn btn-sm btn-warning'><i class='fa fa-pencil-square-o' ></i> Update</button>\":\"\";"
                    . "\n\t\t\t\techo '</form> '; "
                    . "\n\t\t\t\techo (\$Del ==1)? anchor(site_url('".ucfirst($c_url)."/delete/'.$".$c_url."->".$pk."),'<i class=\"fa fa-trash-o\"></i> Delete','title=\"Delete\" class=\"btn btn-danger btn-sm\" onclick=\"javasciprt: return confirm(\\'Are You Sure ?\\')\"'):\"\"; "
                    . "\n\t\t\t\t?>"
                    . "\n\t\t\t</td> \n\t\t</tr> <?php } ?>";
            $string .="
            </table>
          </div><!-- /.box-body -->
          <div class='box-footer'>
            <div class=\"col-md-4\">
            Total Record : <?php echo \$total_rows ?>
            </div>
            <div class=\"col-md-4 text-center\">
              <?php echo \$pagination ?>
            </div>
          </div><!-- box-footer -->
        </div><!-- /.box -->
      </div>
    </div>
  </section><!-- /.content -->
</div><!-- /.content-wrapper -->
<?php if(\$Edi==1 ){?>
<script type=\"text/javascript\">
$(document).ready(function(){
  <?php
  foreach ($" . $c_url . "_data as \$$c_url){ ?>
  ";
foreach ($non_pk as $row) {
  $string .="
  $(\"#tx".$row['column_name']."<?php echo $".$c_url."->".$pk." ?>\").click(function(){
    $(\"#fr".$row['column_name']."<?php echo $".$c_url."->".$pk.";?>\").attr('type','text');
    $(\"#tx".$row['column_name']."<?php echo $".$c_url."->".$pk."; ?>\").hide();
    $(\"#ed<?php echo $".$c_url."->".$pk."; ?>\").prop('disabled', false);
  });
  $(\"#fr".$row['column_name']."<?php echo $".$c_url."->".$pk."; ?>\").dblclick(function(){
    $(\"#fr".$row['column_name']."<?php echo $".$c_url."->".$pk.";?>\").attr('type','hidden');
    $(\"#tx".$row['column_name']."<?php echo $".$c_url."->".$pk."; ?>\").show();
    $(\"#ed<?php echo $".$c_url."->".$pk."; ?>\").prop('disabled', true);
  });
  ";
}
$string .="

  <?php }?>
});
</script>
<?php }  ?>";
$hasil_view_list = createFile($string, $target."views/" . $c_url . "/" . $v_list_file);
?>
