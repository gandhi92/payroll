<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title> Login Payroll </title>
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <link rel="stylesheet" href="<?php echo base_url() ?>template/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?php echo base_url() ?>template/bootstrap/css/code.css">
    <style>
      body{
       background: url('<?= base_url('template/bootstrap/images/sek-bg2.jpg') ?>');
       background-size: cover;
       }
      .panel{
       background-color: rgba(255, 255, 255, 0.2);
      }
      .panel-footer{
       background-color: rgba(255, 255, 255, 0.2);
      }
    </style>  
</head>
<body>
    <div class="container" style="padding-top: 120px;">
      <div class="col-sm-4">        
      </div>      
      <div class="col-sm-4">
       <div class="panel panel-default">
        <div class="panel-heading">Login Pengguna</div>
        <div class="panel-body">
        <?php echo form_open('Welcome/login'); ?>
         <div class="form-group">
          <label class="control-label">Nama Pengguna </label><?php echo form_error('username'); ?>
          <input type="text" name="username" maxlength="25" class="form-control"/>
         </div>
         <div class="form-group">
          <label class="control-label">Password </label><?php echo form_error('password'); ?>
          <input type="password" name="password" class="form-control"/>
         </div>
        </div>
        <div class="panel-footer">
         <input type="submit" name="login" class="btn btn-info" value="Login"/>
        </div>
        <?php echo form_close(); ?>
       </div>
      </div>
      <div class="col-sm-4">        
      </div>      
    </div>
</body>
</html>