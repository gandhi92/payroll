<?php
 $Ses = $this->session->userdata;
 $Con = $Ses['controller'];
 $Cre = $Ses['data']['tambah'];
 $Edi = $Ses['data']['edit'];
 $Del = $Ses['data']['delete'];
 $Rep = $Ses['data']['report'];
?>
<div class='content-wrapper'>
  <section class='content-header'>
    <h1>
      Dashboard
      <small>Menu Management</small>
    </h1>
    <ol class='breadcrumb'>
      <li><a href='#'><i class='fa fa-dashboard'></i> Home</a></li>
      <li class='active'>Menu Management</li>
    </ol>
  </section>

  <!-- Main content -->
  <section class='content'>
    <div class='row'>
      <div class='col-xs-12'>
        <div class='box box-solid box-info'>
          <div class='box-header with-border'>
            <h3 class='box-title'>Menu Management</h3>
          </div><!-- /.box-header -->
          <div class='box-body'>
            <div class="col-md-4"></div>
            <div class="col-md-4 text-center">
                <div style="margin-top: 8px" id="message">
                    <?php echo $this->session->userdata('message') <> '' ? $this->session->userdata('message') : ''; ?>
                </div>
            </div>
            <div class="col-md-4 text-right">
                <form action="<?php echo base_url('index.php/menus_management/index'); ?>" class="form-inline" method="get">
                    <div class="input-group">
                        <input type="text" class="form-control" name="q" value="<?php echo $q; ?>">
                        <span class="input-group-btn">
                            <?php
                                if ($q <> '')
                                {
                                    ?>
                                    <a href="<?php echo base_url('index.php/menus_management'); ?>" class="btn btn-default">Reset</a>
                                    <?php
                                }
                            ?>
                          <button class="btn btn-primary" type="submit">Search</button>
                        </span>
                    </div>
                </form>
            </div>
          </div>
          <div class='box-body'>
            <table class='table table-hover'>
            <tr>
                <th >No</th>
            		<th>Nama Users</th>
                <th>Nama Menu</th>
            		<th>Tambah</th>
            		<th>Edit</th>
            		<th>Delete</th>
            		<th>Report</th>
            		<?php echo ($Edi==0 && $Del==0)?"":"<th>Action</th>"; ?>
            </tr>
            <form role='form' action="<?php echo base_url("index.php/"); ?>Menus_management/create_action" method="post">
            <tr>
              <td></td>
              <td>
                <select name="id_user" class="form-control input-sm">
                  <option> - </option>
                  <?php
                  $Users = $this->db->get('users')->result();
                  foreach ($Users as $key => $value) { ?>
                  <option value="<?php echo $value->id_user; ?>"><?php echo $value->nama_user; ?></option>
                  <?php } ?>
                </select>
              </td>
              <td>
                <select name="id_menus" class="form-control input-sm">
                  <option> - </option>
                  <?php
                  $Users = $this->db->get('menus')->result();
                  foreach ($Users as $key => $value) { ?>
                  <option value="<?php echo $value->id_menu; ?>"><?php echo $value->nama_menu; ?></option>
                  <?php } ?>
                </select>
              </td>
              <td>
                <select name="tambah" class="form-control input-sm">
                  <option value="1"> Ya </option>
                  <option value="2"> Tidak </option>
                </select>
              </td>
              <td>
                <select name="edit" class="form-control input-sm">
                  <option value="1"> Ya </option>
                  <option value="2"> Tidak </option>
                </select>
              </td>
              <td>
                <select name="delete" class="form-control input-sm">
                  <option value="1"> Ya </option>
                  <option value="2"> Tidak </option>
                </select>
              </td>
              <td>
                <select name="report" class="form-control input-sm">
                  <option value="1"> Ya </option>
                  <option value="2"> Tidak </option>
                </select>
              </td>
              <td style="text-align:center">
                <?php echo ($Cre == 1)? "<button type=\"submit\" class=\"btn btn-sm btn-success\"> Tambah </button>":""; ?>
              </td>
            </tr>
          </form>
            <?php
                        foreach ($menus_management_data as $menus_management)
                        {
                            ?>
                            <tr>
			<td width="30px"><?php echo ++$start ?></td>
      <td>
        <select name="id_user" class="form-control input-sm" <?php echo ( $menus_management->id_menus==13 && $menus_management->id_user==$this->session->userdata()['id_user'])?"disabled ":""; ?>
<?php echo ($Edi == 1)? "onchange=\"Update(this,".$menus_management->id.",'id_user')\"":"disabled"; ?>
        >
          <option> - </option>
          <?php
          $User = $this->db->get('users')->result();
          foreach ($User as $key => $value) { ?>
          <option
          value="<?php echo $value->id_user; ?>"
          <?php echo ($value->id_user == $menus_management->id_user)?"selected":""; ?>
          ><?php echo $value->nama_user; ?></option>
          <?php } ?>
        </select>
      </td>
      <td>
        <select name="id_menus" class="form-control input-sm" <?php echo ( $menus_management->id_menus==13 && $menus_management->id_user==$this->session->userdata()['id_user'])?"disabled ":""; ?>
<?php echo ($Edi == 1)? "onchange=\"Update(this,".$menus_management->id.",'id_menus')\"":"disabled"; ?>
        >
          <option> - </option>
          <?php
          $Menu = $this->db->get('menus')->result();
          foreach ($Menu as $key => $value) { ?>
          <option
          value="<?php echo $value->id_menu; ?>"
          <?php echo ($value->id_menu==$menus_management->id_menu)?"selected":""; ?>
          ><?php echo $value->nama_menu; ?></option>
          <?php } ?>
        </select>
      </td>
			<td>
        <select name="tambah" class="form-control input-sm" <?php echo ( $menus_management->id_menus==13 && $menus_management->id_user==$this->session->userdata()['id_user'])?"disabled ":""; ?>
<?php echo ($Edi == 1)? "onchange=\"Update(this,".$menus_management->id.",'tambah')\"":"disabled"; ?>
        >
          <option value="1" <?php echo ($menus_management->tambah ==1)?"selected":""; ?>> Ya </option>
          <option value="2" <?php echo ($menus_management->tambah !=1)?"selected":""; ?>> Tidak </option>
        </select>
      </td>
			<td>
        <select name="edit" class="form-control input-sm" <?php echo ( $menus_management->id_menus==13 && $menus_management->id_user==$this->session->userdata()['id_user'])?"disabled ":""; ?>
<?php echo ($Edi == 1)? "onchange=\"Update(this,".$menus_management->id.",'edit')\"":"disabled"; ?>
        >
          <option value="1" <?php echo ($menus_management->edit ==1)?"selected":""; ?>> Ya </option>
          <option value="2" <?php echo ($menus_management->edit !=1)?"selected":""; ?>> Tidak </option>
        </select>
      </td>
      <td>
        <select name="delete" class="form-control input-sm" <?php echo ( $menus_management->id_menus==13 && $menus_management->id_user==$this->session->userdata()['id_user'])?"disabled ":""; ?>
<?php echo ($Edi == 1)? "onchange=\"Update(this,".$menus_management->id.",'delete')\"":"disabled"; ?>
        >
          <option value="1" <?php echo ($menus_management->delete ==1)?"selected":""; ?>> Ya </option>
          <option value="2" <?php echo ($menus_management->delete !=1)?"selected":""; ?>> Tidak </option>
        </select>
      </td>
      <td>
        <select name="report" class="form-control input-sm"
<?php echo ( $menus_management->id_menus==13 && $menus_management->id_user==$this->session->userdata()['id_user'])?"disabled ":""; ?>
<?php echo ($Edi == 1)? "onchange=\"Update(this,".$menus_management->id.",'report')\"":"disabled"; ?>
        >
          <option value="1" <?php echo ($menus_management->report ==1)?"selected":""; ?>> Ya </option>
          <option value="2" <?php echo ($menus_management->report !=1)?"selected":""; ?>> Tidak </option>
        </select>
      </td>
      <?php if($Edi==1 || $Del==1){?>
			<td style="text-align:center" width="200px">
				<?php
				echo ($Del == 1 && ($menus_management->id_menus != 13 || $menus_management->id_user!=$this->session->userdata()['id_user']) )? anchor(base_url('index.php/Menus_management/delete/'.$menus_management->id),'<i class="fa fa-trash-o"></i> Delete','title="Delete" class="btn btn-danger btn-sm" onclick="javasciprt: return confirm(\'Are You Sure ?\')"'):"";
				?>
			</td>
		</tr> <?php } } ?>
            </table>
          </div><!-- /.box-body -->
          <div class='box-footer'>
            <div class="col-md-4">
            Total Record : <?php echo $total_rows ?>
            </div>
            <div class="col-md-4 text-center">
              <?php echo $pagination ?>
            </div>
          </div><!-- box-footer -->
        </div><!-- /.box -->
      </div>
    </div>
  </section><!-- /.content -->
</div><!-- /.content-wrapper -->
<?php if ($Edi == 1) { ?>
<script type="text/javascript">
  function Update(v,i,f){
    $.ajax({
        type:"POST",
        url:"<?php echo base_url()."index.php/Menus_management/UpdateOption/?"; ?>",
        data:"f="+f+"&i="+i+"&v="+v.value,
        success:function(html){
          $("#message").html("Data Sudah Diupdate");
          location.reload();
        }
    });
  }
</script>
<?php } ?>
