<?php 
$Users = $this->session->userdata();
$Project = $this->session->userdata("project");
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title><?php echo $Project['nama']; ?></title>
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <link rel="stylesheet" href="<?php echo base_url() ?>template/bootstrap/css/bootstrap.min.css">
        <link rel="stylesheet" href="<?php echo base_url() ?>template/font-awesome-4.4.0/css/font-awesome.min.css">
        <link rel="stylesheet" href="<?php echo base_url() ?>template/ionicons-2.0.1/css/ionicons.min.css">
        <link rel="stylesheet" href="<?php echo base_url() ?>template/plugins/datatables/dataTables.bootstrap.css">
        <link rel="stylesheet" href="<?php echo base_url() ?>template/dist/css/AdminLTE.min.css">
        <link rel="stylesheet" href="<?php echo base_url() ?>template/bootstrap/css/bootstrap-datepicker3.css">
        <link rel="stylesheet" href="<?php echo base_url() ?>template/dist/css/skins/_all-skins.min.css">
        <script src="<?php echo base_url() ?>template/plugins/jQuery/jQuery-2.1.4.min.js"></script>
        <script src="<?php echo base_url() ?>template/plugins/jQueryUI/jquery-ui.min.js"></script>
        <script src="<?php echo base_url() ?>template/bootstrap/js/bootstrap.min.js"></script>
        <script src="<?php echo base_url() ?>template/plugins/datatables/jquery.dataTables.min.js"></script>
        <script src="<?php echo base_url() ?>template/plugins/datatables/dataTables.bootstrap.min.js"></script>
        <script src="<?php echo base_url() ?>template/plugins/slimScroll/jquery.slimscroll.min.js"></script>
        <script src="<?php echo base_url() ?>template/plugins/fastclick/fastclick.min.js"></script>
        <script src="<?php echo base_url() ?>template/dist/js/app.min.js"></script>
        <script src="<?php echo base_url() ?>template/dist/js/demo.js"></script>
        <script src="<?php echo base_url() ?>template/bootstrap/js/bootstrap-datepicker.min.js"></script>        
    </head>
    <body class="hold-transition skin-blue fixed sidebar-mini">
        <div class="wrapper">
            <header class="main-header">
                <a href="<?php echo base_url(); ?>" class="logo">
                    <span class="logo-lg"><b><?php echo $Project['nama']; ?></b></span>>
                </a>
                <nav class="navbar navbar-static-top">
                    <div class="navbar-custom-menu">
                        <ul class="nav navbar-nav">
                            <li class="dropdown user user-menu">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                    <img src="<?php echo base_url()?>template/dist/img/user2-160x160.jpg" class="user-image" alt="User Image">
                                    <span class="hidden-xs"><?php echo $Users['nama_user']; ?></span>
                                </a>
                                <ul class="dropdown-menu">
                                    <li class="user-header">
                                        <img src="<?php echo base_url()?>template/dist/img/user2-160x160.jpg" class="img-circle" alt="User Image">
                                        <p>
                                            <?php echo $Users['nama_user']; ?>
                                            <small>Last Login : <?php echo $Users['last_login']; ?></small>
                                        </p>
                                    </li>
                                    <!-- Menu Body -->
                                    <li class="user-body">
                                        <div class="col-xs-4 text-center">
                                            <a href="#"></a>
                                        </div>
                                        <div class="col-xs-4 text-center">
                                            <a href="#"></a>
                                        </div>
                                        <div class="col-xs-4 text-center">
                                            <a href="#"></a>
                                        </div>
                                    </li>
                                    <li class="user-footer">
                                        <div class="pull-left">
                                            <a href="#pMdl" class="btn btn-default btn-flat" data-toggle="modal">Profile</a>
                                        </div>
                                        <div class="pull-right">
                                        <?php
                                        echo anchor('Welcome/Log_out','Sing out',array('class'=>'btn btn-default btn-flat'));
                                        ?>
                                        </div>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                </nav>
            </header>
            <aside class="main-sidebar">
                <section class="sidebar">
                    <?php 
                    $idm = Menus();
                    (empty($idm))? Redirect("Welcome/Log_out"):"";
                    $this->db->where('parent_menu','0');
                    $this->db->where('active','1');
                    $this->db->where_in('id_menu',$idm); 
                    $this->db->order_by('no','ASC');
                    $Menu = $this->db->get('menus')->result();
                    foreach ($Menu as $key => $value) { 
                        $this->db->where('parent_menu',$value->id_menu);
                        $this->db->where('link_menu',$this->uri->segment(1));
                        $this->db->where('active','1');
                        $this->db->where_in('id_menu',$idm); 
                        $C = $this->db->get('menus');
                        ?>
                        <ul class="sidebar-menu data-widget="tree"">
                            <li class="<?php if($C->num_rows()>0){ echo "active";} elseif($this->uri->segment(1)==$value->link_menu){ echo "active";} ?> treeview menu-open">
                              <a href="<?php echo base_url("index.php")."/".$value->link_menu; ?>">
                                <i class="fa <?php echo $value->icon_menu; ?>"></i> <span><?php echo $value->nama_menu; ?></span>
                                <span class="pull-right-container">
                                  <i class=" <?php if($C->num_rows() > 0){ echo "fa fa-angle-left pull-right";}?>"></i>
                                </span>
                              </a>
                              <?php
                                $this->db->where('parent_menu',$value->id_menu);
                                $this->db->where('active','1');
                                $this->db->where_in('id_menu',$idm); 
                                $Sub_menu = $this->db->get('menus');                              
                                foreach ($Sub_menu->result() as $ke => $valu) { ?>
                                  <ul class="treeview-menu">
                                    <li <?php if ($this->uri->segment(1) == $valu->link_menu) { echo "class='active'"; $statusMenu='active';} ?>>
                                        <a href="<?php echo base_url("index.php")."/".$valu->link_menu; ?>">
                                        <i class="fa <?php echo $valu->icon_menu; ?>"></i> <?php echo $valu->nama_menu; ?></a></li>
                                  </ul>
                              <?php } ?>
                            </li>
                        </ul>
                    <?php } ?>
                </section>
            </aside>            
            <?php
            echo $contents;
            ?>
            <div class="modal fade" id="pMdl" role="dialog">
              <div class="modal-dialog">
                <div class='box box-success'>
                  <div class='box-header'>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                      <ul class="nav nav-tabs">
                        <li class="active"><a data-toggle="tab" href="#menu1" class="btn btn-sm btn-success"> Ubah Nama </a></li>
                        <li><a data-toggle="tab" href="#menu2" class="btn btn-sm btn-success"> Ubah User Login </a></li>
                        <li><a data-toggle="tab" href="#menu3" class="btn btn-sm btn-success"> Ubah Password </a></li>
                      </ul>
                  </div>
                  <div class="box-body">
                      <div class="tab-content">
                        <div id="menu1" class="tab-pane fade in active">
                           <?php echo form_open("Welcome/Rename"); ?>
                              <div class='form-group'>
                                Nama 
                                  <input type="text" class="form-control" name="nama_user" value="<?php echo $Users['nama_user']; ?>" />
                              </div>
                              <div class='form-group'>
                                <button type="submit" name="Rename" class="btn btn-sm btn-success"> Ubah Nama </button>
                              </div>
                          </form>
                        </div>
                        <div id="menu2" class="tab-pane fade">
                           <?php echo form_open("Welcome/Luser"); ?>
                              <div class='form-group'>
                                Login User 
                                  <input type="text" class="form-control" name="login_user" value="<?php echo $Users['login_user']; ?>" />
                              </div>
                              <div class='form-group'>
                                Recent Password 
                                  <input type="password" class="form-control" name="pwd" required />
                              </div>                              
                              <div class='form-group'>
                                <button type="submit" name="UserLogin" class="btn btn-sm btn-success"> Ubah Login User </button>
                              </div>
                          </form>
                        </div>
                        <div id="menu3" class="tab-pane fade">
                           <?php echo form_open("Welcome/Lpwd"); ?>
                              <div class='form-group'>
                                Recent Password 
                                  <input type="password" class="form-control" name="op" required/>
                              </div>
                              <div class='form-group'>
                                New Password
                                  <input id="n" type="password" class="form-control" name="np" required/>
                              </div>
                              <div class='form-group'>
                                Confirm Password <font color="red"><i id="cpwd" ></i></font>
                                  <input id="c" type="password" class="form-control" name="cp" required/>
                              </div> 
                              <div class='form-group'>
                                <button type="submit" name="UserLogin" class="btn btn-sm btn-success"> Ubah Password </button>
                              </div>
                          </form>
                        </div>                        
                      </div>                  
                  </div>
                </div>
              </div>
            </div>            
            <footer class="main-footer">
                <div class="pull-right hidden-xs">
                    <b>Version</b> <?php echo $Project['version']; ?>
                </div>
                <strong>
                  Copyright <?php echo "@ ".$Project['copyright']; ?>
            </footer>
            <div class="control-sidebar-bg"></div>
        </div>        
        <script>
          $(document).ready(
            function(){
              $('#c').keyup(
                function(){
                  if ($('#n').val() != $('#c').val()) {
                    $('#cpwd').html("Password Belum Cocok !");
                  }
                  else{
                    $('#cpwd').html("");
                  }                  
                }
              );

            }
          );
            $(document).ready(function(){
              var date_input=$('input[name="tgl_lahir"]');
              var container=$('.bootstrap-iso form').length>0 ? $('.bootstrap-iso form').parent() : "body";
              var options={
                format: 'yyyy-mm-dd',
                container: container,
                autoclose: true,
                todayHighlight: true,
                orientation: 'top'
              };
              date_input.datepicker(options);             
            }) 

            $(function () {
                $("#example1").DataTable();
                $('#example2').DataTable({
                    "paging": true,
                    "lengthChange": false,
                    "searching": false,
                    "ordering": true,
                    "info": true,
                    "autoWidth": false
                });
            });
        </script>
    </body>
</html>
