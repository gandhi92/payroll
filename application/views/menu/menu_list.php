<?php
 $Ses = $this->session->userdata; 
 $Con = $Ses['controller'];
 $Cre = $Ses['data']['tambah'];
 $Edi = $Ses['data']['edit'];
 $Del = $Ses['data']['delete'];
 $Rep = $Ses['data']['report'];
?>
<div class='content-wrapper'>
  <section class='content-header'>
    <h1>
      Dashboard
      <small>Menu List</small>
    </h1>
    <ol class='breadcrumb'>
      <li><a href='#'><i class='fa fa-dashboard'></i> Home</a></li>
      <li class='active'>Menu List</li>
    </ol>
  </section>
  <!-- Main content -->
  <section class='content'>
    <div class='row'>
      <div class='col-xs-12'>
        <div class='box box-solid box-info'>
          <div class='box-header with-border'>
            <h3 class='box-title'>Menu List</h3>
          </div><!-- /.box-header -->

          <div class='box-body'>
            <div class="col-md-4">
                <?php echo ($Cre == 1)? "<button type=\"button\" class=\"btn btn-success btn-sm\" data-toggle=\"modal\" data-target=\"#cModal\"> Tambah </button>":""; ?>
            </div>
            <div class="col-md-4 text-center">
                <div style="margin-top: 8px" id="message">
                    <?php echo $this->session->userdata('message') <> '' ? $this->session->userdata('message') : ''; ?>
                </div>              
            </div>
          </div>
          <div class='box-body'>

          <table class="table table-bordered table-striped" id="mytable">
                      <thead>
                          <tr>
                              <th width="10px">No</th>
		    <th>Nama Menu</th>
		    <th>Link Menu</th>
		    <th>Icon Menu</th>
		    <th>Sub Dari</th>
		    <th>Status</th>
        <?php echo ($Edi==0 && $Del==0)?"":"<th>Action</th>"; ?>
		    
                          </tr>
                      </thead>
	    <tbody>
                      <?php
                      $start = 0;
                      foreach ($menu_data as $menu)
                      {
                          ?>
                          <tr>
		    <td><?php echo ++$start ?></td>
		    <td><?php echo $menu->nama_menu ?></td>
		    <td><?php echo $menu->link_menu ?></td>
		    <td><i class=" fa <?php echo $menu->icon_menu ?>"></i></td>
		    <td>
          <?php
          $this->db->select('nama_menu');
          $this->db->where('id_menu',$menu->parent_menu);
          $NamaMenu = $this->db->get('menus')->row_array();
          echo $NamaMenu['nama_menu']; ?>
          </td>
		    <td>
          <?php
          if ($menu->active == 1) {
            echo "Active";
          }
          else{
            echo "Tidak Aktive";
          }
          ?>
          
        </td>
        <?php if($Edi==1 || $Del==1){?>
		    <td style="text-align:center" width="180px">
			<?php 
      echo ($Edi == 1)? anchor("#eModal",'<i class="fa fa-pencil-square-o"></i> Update',array('title'=>'edit','class'=>'btn btn-warning btn-sm','data-toggle'=>'modal','data-id'=>$menu->id_menu)):""; 
			echo '  '; 
			echo ($Del ==1)?anchor(site_url('menu/delete/'.$menu->id_menu),'<i class="fa fa-trash-o"></i> Delete','title="delete" class="btn btn-danger btn-sm" onclick="javasciprt: return confirm(\'Are You Sure ?\')"'):""; 
			?>
		    </td>
	        </tr>
                          <?php }
                      }
                      ?>
                      </tbody>
                  </table>
                  <script type="text/javascript">
                      $(document).ready(function () {
                          $("#mytable").dataTable();
                      });
                  </script>

          </div><!-- /.box-body -->
        </div><!-- /.box -->
      </div>
    </div>
    <div class="modal fade" id="cModal" role="dialog">
      <div class="modal-dialog">
            <div class='box box-solid box-info'>
              <div class='box-header'>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h3 class='box-title'>Form Input MENU</h3>
              </div><!-- /.box-header -->
              <!-- form start -->
                <form role='form' action="<?php echo base_url('index.php/Menu/create_action'); ?>" method="post">
                <div class='box-body'>  
                  <div class='form-group'>
                    Nama Menu <?php echo form_error('nama_menu') ?>
                      <input type="text" class="form-control" name="nama_menu" id="nama_menu" placeholder="Nama Menu" value="" />
                  </div>

                  <div class='form-group'>
                    Link Menu <?php echo form_error('link_menu') ?>
                      <input type="text" class="form-control" name="link_menu" id="link_menu" placeholder="Link Menu" value="" />
                  </div>

                  <div class='form-group'>
                    Icon Menu <?php echo form_error('icon_menu') ?> 
                      <input type="text" class="form-control" name="icon_menu" id="icon_menu" placeholder="Icon Menu" value="" />
                  </div>

                  <div class='form-group'>
                    Sub Menu Dari <?php echo form_error('parent_menu') ?>
                    <select class="form-control" name="parent_menu" id="parent_menu">
                      <option value="0" default> - </option>
                      <?php
                      $Menu = $this->db->get('menus')->result();
                      foreach ($Menu as $key => $value) {?>                                
                      <option value="<?php echo $value->id_menu; ?>"><?php echo $value->nama_menu; ?></option>
                      <?php } ?>
                    </select>
                  </div>

                  <div class='form-group'>
                    Active <?php echo form_error('active') ?>
                    <select class="form-control" name="active" id="active">
                      <option value="1" > Active </option>
                      <option value="0" > Tidak Aktive </option>
                    </select>
                  </div>
                </div><!-- /.box-body -->
                <div class='box-footer'>
                <button type="submit" class="btn btn-primary pull-right"> Simpan </button>
                </div><!-- /.box-footer -->
              </form>
            </div>
      </div>      
    </div>
    <div class="modal fade" id="eModal" role="dialog">
      <div class="modal-dialog">
      <div id="EditData"></div>        
      </div>
    </div>

  </section><!-- /.content -->
</div><!-- /.content-wrapper -->

  <script type="text/javascript">
    $(document).ready(function(){
        $('#eModal').on('show.bs.modal', function (e) {
            var idx = $(e.relatedTarget).data('id');
            //menggunakan fungsi ajax untuk pengambilan data
            $.ajax({
                type : 'post',
                url : '<?php echo base_url("index.php/Menu/update") ?>',
                data :  'id='+ idx,
                success : function(data){
                $('#EditData').html(data);//menampilkan data ke dalam modal
                }
            });
         });
    });
  </script>