<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Menus_management_model extends CI_Model
{

    public $table = 'menus_management';
    public $id = 'id';
    public $order = 'DESC';

    function __construct(){
        parent::__construct();
    }

    // get all
    function get_all() {
        $this->db->join('users','users.id_user = menus_management.id_user');
        $this->db->join('menus','menus_management.id_menus = menus.id_menu');
        $this->db->order_by($this->id, $this->order);        
        return $this->db->get($this->table)->result();
    }

    // get data by id
    function get_by_id($id){
        $this->db->join('menus','menus_management.id_menus = menus.id_menu');
        $this->db->join('users','users.id_user = menus_management.id_user');
        $this->db->where($this->id, $id);
        return $this->db->get($this->table)->row();
    }
    // get total rows
    function total_rows($q = NULL) {
        $this->db->like('menus_management.id', $q);
        $this->db->or_like('menus.nama_menu', $q);
        $this->db->or_like('menus_management.id_menus', $q);
        $this->db->or_like('menus_management.tambah', $q);
        $this->db->or_like('menus_management.edit', $q);
        $this->db->or_like('menus_management.delete', $q);
        $this->db->or_like('menus_management.report', $q);
    	$this->db->from($this->table);
        $this->db->join('users','users.id_user = menus_management.id_user');
        $this->db->join('menus','menus_management.id_menus = menus.id_menu');        
        return $this->db->count_all_results();
    }
    // get data with limit and search
    function get_limit_data($limit, $start = 0, $q = NULL) {
        $this->db->join('menus','menus_management.id_menus = menus.id_menu');
        $this->db->join('users','users.id_user = menus_management.id_user');  
        $this->db->order_by($this->id, $this->order);
        $this->db->like('menus_management.id', $q);
    	$this->db->or_like('menus.nama_menu', $q);
    	$this->db->or_like('menus_management.id_menus', $q);
    	$this->db->or_like('menus_management.tambah', $q);
    	$this->db->or_like('menus_management.edit', $q);
    	$this->db->or_like('menus_management.delete', $q);
    	$this->db->or_like('menus_management.report', $q);
    	$this->db->limit($limit, $start);
        return $this->db->get($this->table)->result();
    }

    // insert data
    function insert($data){
        $this->db->insert($this->table, $data);
    }
    // update data
    function update($id, $data){
        $this->db->where($this->id, $id);
        $this->db->update($this->table, $data);
    }
    // delete data
    function delete($id){
        $this->db->where($this->id, $id);
        $this->db->delete($this->table);
    }
}