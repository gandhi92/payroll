<?php

if (!defined('BASEPATH')){ exit('No direct script access allowed'); }

class Welcome_model extends CI_Model{
	function Rename($id,$name){
		$this->db->where("id_user",$id);
		
		if ($this->db->update("users",array("nama_user"=>$name))) {
			return TRUE;
		}
		else{
			return FALSE;
		}
	}
	function UbahUlogin($id,$login){
		$this->db->where("id_user",$id);
		if ($this->db->update("users",array("login_user"=>$login))) {
			return TRUE;
		}
		else{
			return FALSE;
		}		
	}
	function UbahUpwd($id,$pwd){
		$this->db->where("id_user",$id);
		if ($this->db->update("users",array("password_user"=>password_hash($pwd,PASSWORD_DEFAULT)))) {
			return TRUE;
		}
		else{
			return FALSE;
		}		
	}
	function updateTime($id){
		$this->db->where("id_user",$id);
		if ($this->db->update("users",array("last_login"=>date('Y-m-d H:i:s')))) {
			return TRUE;
		}
		else{
			return FALSE;
		}
	}
}
?>