<?php
function autocomplete_json($table,$field){
    $ci = get_instance();
    $ci->db->like('nama_pengajar', $_GET['term']);
    $ci->db->or_like('nip_pengajar', $_GET['term']);
    $collections = $ci->db->get($table)->result();
    foreach ($collections as $collection) {
        $return_arr[]  = array($collection->nip_pengajar,$collection->nama_pengajar);
    }
    echo json_encode($return_arr);
}
function SetProject(){
    $ci = get_instance();
    $P = $ci->db->get('project')->row_array();
    $D = array(
        "version"=>$P['version'],
        "copyright"=>$P['copyright'],
        "nama"=>$P['nama']
    );
    $ci->session->set_userdata(array("project"=>$D));
}
function Test($data){ // test login
    $ci = get_instance();
    $ci->db->select('*');
    $ci->db->where('login_user',$data['username']);
    $D = $ci->db->get('users')->row();
    if (empty($D)) {
        $R['result'] = NULL;
        $R['token'] = NULL;
        $R['data'] = array();
    }
    else{
        $ci->db->where("id_user",$D->id_user);
        $DT = $ci->db->get('users')->row_array();
        $SD = array(
                    "id_user" =>$DT['id_user'],
                    "login_user" =>$DT['login_user'],
                    "password_user" =>$DT['password_user'],
                    "nama_user" =>$DT['nama_user'],
                    "last_login" =>$DT['last_login'],
                    "token" => password_hash($DT['login_user'],PASSWORD_DEFAULT)
        );
        $R['result'] =  password_verify($data['password'],$D->password_user);
        $R['data'] = $SD;
    }
    return $R;
}
function OtenUser($pash){
    $ci = get_instance();
    $S = $ci->session->userdata();
    if (password_verify($pash,$S['token']) == 1) {
        return TRUE;
    }
    else{
        return FALSE;
    }
}
function CekLogin(){ // cek login
    $ci = get_instance();
    $session = $ci->session->userdata();
    if (isset($session['id_user'])) {
        $ci->db->where(array('id_user'=>$session['id_user'],'password_user'=>$session['password_user']));
        $D = $ci->db->get('users')->num_rows();
        if ($D==1) {
            return TRUE;
        }
        else{
            return FALSE;
        }        
    }
    else{
        return FALSE;
    }
}
function Logout(){
    $ci = get_instance();
    $ci->session->sess_destroy();
}
function Menus(){
    // menampilkan menu yg hanya di ijinkan
    $ci = get_instance();
    $session = $ci->session->userdata();
    $Query = "SELECT id_menus FROM `menus_management` JOIN `menus` ON menus.id_menu=menus_management.id_menus WHERE menus_management.id_user='{$session['id_user']}'";
    $Exe = $ci->db->query($Query);
    $IdMenus = $Exe->result();
    $idm = array();
    foreach ($IdMenus as $key => $value) {
        $idm[] = $value->id_menus;
    }
    return $idm;
}

function CRUD($controller){
    // disable fungsi crud / hidden fungsi crud sesuai kebutuhan
    $ci = get_instance();
    $session = $ci->session->userdata();
    $Query = "SELECT * FROM `menus_management` JOIN `menus` 
    ON menus.id_menu=menus_management.id_menus 
    WHERE menus_management.id_user='{$session['id_user']}' 
    AND menus.link_menu='{$controller}'";
    $Exe = $ci->db->query($Query);
    $D = $Exe->row_array();
    $data = array(
        "tambah"=>$D['tambah'],
        "edit"=>$D['edit'],
        "delete"=>$D['delete'],
        "report"=>$D['report']

    );
    return $data;     
}
function SetCrud($controller){
    $ci = get_instance();
    $ci->session->unset_userdata(array("controller","data"));
    $ci->session->set_userdata(array("controller"=>$controller,"data"=>CRUD($controller)));    
}
function CekMenu($controller){
    $ci = get_instance();
    $session = $ci->session->userdata();    
    $Query = "SELECT * FROM `menus_management` JOIN `menus` 
    ON menus.id_menu=menus_management.id_menus 
    WHERE menus_management.id_user='{$session['id_user']}'
    AND menus.link_menu='{$controller}'";
    $Exe = $ci->db->query($Query);
    $Result = $Exe->num_rows();
    if ($Result > 0) {
        return TRUE;
    }
    else{
        return FALSE;
    }
}
?>