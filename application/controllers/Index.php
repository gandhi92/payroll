<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Index extends CI_Controller {
    function __construct(){
        parent::__construct();
        SetProject();
    }	
	public function index(){
		redirect("Index/dashboard");
	}
	public function dashboard(){
		if (CekLogin()) {
			$this->template->load('template','index/dashboard');
		}
		else{
			$this->load->view('login');
		}
		
	}
}