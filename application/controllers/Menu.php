<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Menu extends CI_Controller{
    function __construct()    {
        parent::__construct();
        $this->load->model('Menu_model');
        $this->load->library('form_validation');
        if (!CekMenu("Menu")) {
            redirect(base_url());
        }
        SetCrud("Menu");
    }

    public function index() {
        if (CekLogin()) {
            $menu = $this->Menu_model->get_all();
            $data = array( 'menu_data' => $menu );
            $this->template->load('template','menu/menu_list', $data);
        }
        else{
            $this->load->view('login');
        }

    }

    public function create_action()    {
        $this->_rules();
        if ($this->input->post('nama_menu')== NULL || $this->input->post('link_menu')== NULL) {
            $this->session->set_flashdata('message', 'Failed Import Menu');
            redirect(site_url('Menu'));
        } else {

            $data = array(
            'no'=>$this->Menu_model->getLastNo()['no'],
        		'nama_menu' => $this->input->post('nama_menu',TRUE),
        		'link_menu' => $this->input->post('link_menu',TRUE),
        		'icon_menu' => $this->input->post('icon_menu',TRUE),
        		'parent_menu' => $this->input->post('parent_menu',TRUE),
        		'active' => $this->input->post('active',TRUE),
        	    );
            $this->Menu_model->insert($data);
            $this->session->set_flashdata('message', 'Create Record Success');
            redirect(site_url('Menu'));
        }
    }

    public function update(){
        $row = $this->Menu_model->get_by_id($this->input->post("id"));

        if ($row) {// kalo ada datanya ?>
            <div class='box box-solid box-info'>
              <div class='box-header'>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h3 class='box-title'>Form Edit MENU</h3>
              </div><!-- /.box-header -->
              <!-- form start -->
                <form role='form' action="<?php echo base_url('index.php/Menu/update_action'); ?>" method="post">
                    <input type="hidden" name="id_menu" value="<?php echo $row->id_menu; ?>">
                <div class='box-body'>  
                  <div class='form-group'>
                    Nama Menu <?php echo form_error('nama_menu') ?>
                      <input type="text" class="form-control" name="nama_menu" id="nama_menu" placeholder="Nama Menu" value="<?php echo $row->nama_menu; ?>">
                  </div>

                  <div class='form-group'>
                    Link Menu <?php echo form_error('link_menu') ?>
                      <input type="text" class="form-control" name="link_menu" id="link_menu" placeholder="Link Menu" value="<?php echo $row->link_menu; ?>">
                  </div>

                  <div class='form-group'>
                    Icon Menu <?php echo form_error('icon_menu') ?> <i class="fa <?php echo $row->icon_menu; ?>"></i>
                      <input type="text" class="form-control" name="icon_menu" id="icon_menu" placeholder="Icon Menu" value="<?php echo $row->icon_menu; ?>">
                  </div>

                  <div class='form-group'>
                    Sub Dari Menu<?php echo form_error('parent_menu') ?>
                    <select class="form-control" name="parent_menu" id="parent_menu">
                      <option value="0" default> - </option>
                      <?php
                      $Menu = $this->db->get('menus')->result();
                      foreach ($Menu as $key => $value) {?>                                
                      <option value="<?php echo $value->id_menu; ?>" <?php echo ($value->id_menu==$row->parent_menu)?"selected":""; ?>><?php echo $value->nama_menu; ?></option>
                      <?php } ?>
                    </select>
                  </div>
                  <div class='form-group'>
                    Status Menu <?php echo form_error('active') ?>
                    <select class="form-control" name="active" id="active">
                      <option value="1" <?php echo ($row->active == 1)?"selected":""; ?>> Active </option>
                      <option value="0" <?php echo ($row->active != 1)?"selected":""; ?>> Tidak Aktive </option>
                    </select>
                  </div>
                </div><!-- /.box-body -->
                <div class='box-footer'>
                <button type="submit" class="btn btn-primary pull-right"> Simpan </button>
                </div><!-- /.box-footer -->
              </form>
            </div>
       <?php
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('Menu'));
        }
    }

    public function update_action(){
        $this->_rules();
        if ($this->form_validation->run() == FALSE) {
            $this->session->set_flashdata('message', "Some Field Can't be Empty");
            redirect(site_url('Menu'));
        } else {
            $data = array(
		'nama_menu' => $this->input->post('nama_menu',TRUE),
		'link_menu' => $this->input->post('link_menu',TRUE),
		'icon_menu' => $this->input->post('icon_menu',TRUE),
		'parent_menu' => $this->input->post('parent_menu',TRUE),
		'active' => $this->input->post('active',TRUE),
	    );

            $this->Menu_model->update($this->input->post('id_menu', TRUE), $data);
            $this->session->set_flashdata('message', 'Update Record Success');
            redirect(site_url('Menu'));
        }
    }

    public function delete($id)
    {
        $row = $this->Menu_model->get_by_id($id);

        if ($row) {
            $this->Menu_model->delete($id);
            $this->session->set_flashdata('message', 'Delete Record Success');
            redirect(site_url('Menu'));
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('Menu'));
        }
    }

    public function _rules()    {
	$this->form_validation->set_rules('nama_menu', 'nama menu', 'trim|required');
	$this->form_validation->set_rules('link_menu', 'link menu', 'trim|required');
	$this->form_validation->set_rules('icon_menu', 'icon menu', 'trim|required');
	$this->form_validation->set_rules('parent_menu', 'parent menu', 'trim|required');
	$this->form_validation->set_rules('active', 'active', 'trim|required');
	$this->form_validation->set_rules('id_menu', 'id_menu', 'trim');
	$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

}