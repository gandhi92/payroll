<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Menus_management extends CI_Controller
{
    function __construct(){
        parent::__construct();
        $this->load->model('Menus_management_model');
        $this->load->library('form_validation');
        if (!CekMenu("Menus_management")) {
            redirect(base_url());
        }
        SetCrud("Menus_management");
    }
    public function index() {
        $q = urldecode($this->input->get('q', TRUE));
        $start = intval($this->input->get('start'));

        if ($q <> '') {
            $config['base_url'] = base_url() . 'Menus_management?q=' . urlencode($q);
            $config['first_url'] = base_url() . 'Menus_management?q=' . urlencode($q);
        } else {
            $config['base_url'] = base_url() . 'Menus_management';
            $config['first_url'] = base_url() . 'Menus_management';
        }

        $config['per_page'] = 10;
        $config['page_query_string'] = TRUE;
        $config['total_rows'] = $this->Menus_management_model->total_rows($q);
        $menus_management = $this->Menus_management_model->get_limit_data($config['per_page'], $start, $q);

        $this->load->library('pagination');
        $this->pagination->initialize($config);

        $data = array(
            'menus_management_data' => $menus_management,
            'q' => $q,
            'pagination' => $this->pagination->create_links(),
            'total_rows' => $config['total_rows'],
            'start' => $start,
        );
        $this->template->load('template','menus_management/menus_management_list', $data);
    }
    public function create_action(){
        $this->_rules();

        if ($this->input->post('id_user') == '-' || $this->input->post('id_menus') == '-') {
            $this->session->set_flashdata('message', 'Nama User Dan Menu Tidak Boleh Kosong !');
            redirect(site_url('Menus_management'));
        } else {
            $data = array(
        		'id_user' => $this->input->post('id_user',TRUE),
        		'id_menus' => $this->input->post('id_menus',TRUE),
        		'tambah' => $this->input->post('tambah',TRUE),
        		'edit' => $this->input->post('edit',TRUE),
        		'delete' => $this->input->post('delete',TRUE),
        		'report' => $this->input->post('report',TRUE),
        	    );

            $this->Menus_management_model->insert($data);
            $this->session->set_flashdata('message', 'Create Record Success');
            redirect(site_url('Menus_management'));
        }
    }
    public function UpdateOption(){
        $id = $this->input->post("i");
        $value = $this->input->post("v");
        $field = $this->input->post("f");
        if ($this->input->post('f')=='tambah') {
            $data = array("tambah"=>$value);
        }
        else if ($this->input->post('f')=='edit') {
            $data = array("edit"=>$value);
        }
        else if ($this->input->post('f')=='delete') {
            $data = array("delete"=>$value);
        }
        else if ($this->input->post('f')=='report') {
            $data = array("report"=>$value);
        }
        else if ($this->input->post('f')=='id_menus') {
            $data = array("id_menus"=>$value);
        }
        else if ($this->input->post('f')=='id_user') {
            $data = array("id_user"=>$value);
        }
        $this->Menus_management_model->update($this->input->post('i', TRUE), $data);        
    }

    public function delete($id){
        $row = $this->Menus_management_model->get_by_id($id);
        if ($row) {
            $this->Menus_management_model->delete($id);
            $this->session->set_flashdata('message', 'Delete Record Success');
            redirect(site_url('Menus_management'));
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('Menus_management'));
        }

    }

    public function _rules(){
	$this->form_validation->set_rules('id_user', 'id user', 'trim|required');
	$this->form_validation->set_rules('id_menus', 'id menus', 'trim|required');
	$this->form_validation->set_rules('tambah', 'tambah', 'trim|required');
	$this->form_validation->set_rules('edit', 'edit', 'trim|required');
	$this->form_validation->set_rules('delete', 'delete', 'trim|required');
	$this->form_validation->set_rules('report', 'report', 'trim|required');
	$this->form_validation->set_rules('id', 'id', 'trim');
	$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

}
