<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {
    function __construct(){
        parent::__construct();
        $this->load->library('form_validation');
        $this->load->model("Welcome_model");
    }	
	public function index(){

		if (CekLogin()) {
			$this->template->load('template','index/dashboard');
		}
		else{
			$this->load->view('login');
		}	
		
	}
	public function login(){
		$this->form_validation->set_rules('username', '', 'required');
		$this->form_validation->set_rules('password', '', 'required');
		if ($this->form_validation->run() == FALSE) {
			$this->load->view('login');
		}
		else{
			$test['username'] = $this->input->post('username');
			$test['password'] = $this->input->post('password');
			$result = Test($test);
			if ($result['result'] == 1) {
				$this->session->set_userdata($result['data']);
				$this->Welcome_model->updateTime($result['data']['id_user']);
				redirect("Index");
			}
			else{
				$this->load->view('login');
			}
		}
	}
	public function Rename(){
		$Ren = $this->Welcome_model->Rename($this->session->userdata()['id_user'],$this->input->post('nama_user'));
		if ($Ren) {
			$this->session->userdata();
			$this->session->set_userdata('nama_user',$this->input->post('nama_user'));
			redirect(base_url());
		}
		else{
			redirect(base_url());
		}
	}
	public function Luser(){
		$data = array(
			"username"=>$this->session->userdata()['login_user'],
			"password"=>$this->input->post("pwd")
		);
		if (Test($data)['result']==1) {
			$Ren = $this->Welcome_model->UbahUlogin($this->session->userdata()['id_user'],$this->input->post('login_user'));
			if ($Ren) {
				$this->session->userdata();
				$this->session->set_userdata('login_user',$this->input->post('login_user'));
				redirect(base_url());
			}
			else{
				redirect(base_url());
			}
		}
		else{
			redirect(base_url());
		}		

	}
	public function Lpwd(){
		$data = array(
			"username"=>$this->session->userdata()['login_user'],
			"password"=>$this->input->post("op")
		);
		if (Test($data)['result']==1) {
			$Ren = $this->Welcome_model->UbahUpwd($this->session->userdata()['id_user'],$this->input->post('cp'));
			if ($Ren) {
				$this->session->userdata();
				$this->session->set_userdata('password_user',password_hash($this->input->post('cp'),PASSWORD_DEFAULT));
				$this->Log_out();
//				redirect(base_url());
			}
			else{
				redirect(base_url());
			}
		}
		else{
			redirect(base_url());
		}		
//		op,np,cp
	}
	public function Log_out(){
		Logout();
		redirect(base_url());
	}
}
